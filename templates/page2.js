let plan = [];
document.addEventListener("DOMContentLoaded", function () {
  var cards = document.querySelectorAll(".card1, .card2, .card3");

  var defaultSelection = document.querySelector(".card1");

  if (defaultSelection) {
    defaultSelection.classList.add("selected");
  }

  let h2tags = ["Arcade", "Advanced", "Pro"];
  let monthlyprices = [9, 12, 15];
  let yearlyprices = [90, 120, 150];
  const checkbox = document.getElementById("checkbox");

  checkbox.addEventListener("change", function updatePrice() {
    const priceelement = document.querySelectorAll('[id^="price"]');
    const freeelement = document.querySelectorAll('[id^="free"]');

    priceelement.forEach((element, index) => {
      if (checkbox.checked) {
        element.textContent = `$${yearlyprices[index]}/yr`;
      } else {
        element.textContent = `$${monthlyprices[index]}/mo`;
      }
    });

    freeelement.forEach((element, index) => {
      if (checkbox.checked) {
        element.textContent = "2 months free";
      } else {
        element.textContent = "";
      }
    });
    // updatePrices();
  });

  cards.forEach(function (card, index) {
    card.addEventListener("click", function () {
      cards.forEach(function (c) {
        c.classList.remove("selected");
      });

      card.classList.add("selected");

      plan = [];

      if (card.classList.contains("selected")) {
        plan.push({
          plan: h2tags[index],
          price: checkbox.checked ? yearlyprices[index] : monthlyprices[index],
          type: checkbox.checked ? "yearly" : "monthly",
        });
      }
      console.log(plan);
      localStorage.setItem("SelectedPlan", JSON.stringify(plan));
    });
  });

  

 
});



